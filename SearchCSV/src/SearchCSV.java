import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class SearchCSV 
{

	public static void main(String args[]) 
	{
		File csvFile = new File("nyt_bestsellers.csv");
		
		if (csvFile.isFile()) 
		{
		    System.out.println("CSV file found.");
		    System.out.println("\n***************");
		    Choice();
			Search();
			
		}
		else
		{
			System.out.println("Sorry! File not found.");
		}
		System.out.println("\n***************");
		
		
	}
	public static void Choice() 
	{
		System.out.println("\nEnter 1 to search title");
		System.out.println("Enter 2 to search genre");
	}
	public static void Search()
	{
		try
		{
		BufferedReader csvReader = new BufferedReader(new FileReader("nyt_bestsellers.csv"));
		
		String row;
		Scanner scan =  new Scanner(System.in);
		System.out.println("\nEnter your choice");
		String choice = scan.nextLine();
		
		if(choice.equals("1")) 
		{
		
		System.out.println("Enter book name within double quotations");
		String name = scan.nextLine();
		
		while ((row = csvReader.readLine()) != null) 
		{
		    String[] data = row.split(",");
		    
		    if (name.equals(data[1]))
		    {
		    System.out.println("\"S.No\",\"BookName\",\"Genre\"");
		    System.out.println(row);
		    }
		    
		}
		}
				
		else if(choice.equals("2"))
		{
			
			System.out.println("Enter genre within double quotations");
			String genre = scan.nextLine();
			System.out.println("\"S.No\",\"Book_Name\",\"Genre\"");
			
			while ((row = csvReader.readLine()) != null) 
			{
			    String[] data = row.split(",");
			    
			    if (genre.equals(data[2]))
			    {
			    System.out.println(row);
			    }
			}
		}
		}	
		
		
			catch(Exception e)
			{
				System.out.println(e);
			}
	}
}
